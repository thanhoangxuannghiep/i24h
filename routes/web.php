<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', "AppController@index");
Route::get('/signup', "AppController@signup");
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/', 'AppController@store');
Route::post('/updateLinkPosition', 'AppController@updateLinkPosition');
Route::delete('/{id}', 'AppController@destroy');
Route::put('/{id}', 'AppController@update');
Route::pattern('urlSlug', '[a-z\-]+');
Route::pattern('id', '[0-9\-]+');
Route::get('/{urlSlug}.html', 'PageController@post');