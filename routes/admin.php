<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/asim/login');
});
Route::get('a-box/position', 'BoxController@getPosition');
Route::post('a-box/list', 'BoxController@getList');
Route::post('a-link/list', 'LinkController@getList');
Route::get('a-box/getList', 'BoxController@resources');
Route::get('a-user/listType', 'UserController@getListType');
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();
Route::get('/home', 'AdminController@index')->name('admin.home');
Route::resource('a-user', 'UserController');
Route::resource('a-box', 'BoxController');
Route::resource('a-link', 'LinkController');
Route::resource('a-setting', 'SettingController');
Route::resource('a-page', 'PageController');
Route::resource('a-sites', 'SiteController');
Route::get('/a/{vue_capture?}', 'AdminController@index')->name('admin.home')->where('vue_capture', '[\/\w\.-]*');
