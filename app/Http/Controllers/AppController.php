<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Link;
use App\Models\Box;
use App\Models\Setting;
use App\Models\Counter;
use App\Models\User;
use Carbon\Carbon;
use Auth;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ip= $request->ip();
        $link = Link::whereCategory_id(12)->whereEnable(1)->where(function($query){
            if(empty(Auth::user())){
                $query->where('user_id', 1);    
            }
            else{
                $query->whereIn('user_id', [1, Auth::user()->id]);
            }
        })->orderBy('priority', 'desc')->get();
        
        if(!empty(Auth::user())){
            if ($request->session()->has(Auth::user()->id.'_'.Auth::user()->firstname)) {
                $linkSession = $request->session()->get(Auth::user()->id.'_'.Auth::user()->firstname);
                $filtered = $link->whereNotIn('id', $linkSession->toArray());
                $link = $filtered;
            }
        }        
        $main = Box::wherePosition(2)->whereStatus(1)->with('links')->get();
        $left = Box::wherePosition(3)->whereStatus(1)->with('links')->get();
        $settings = Setting::all();

        $userSession = $request->session()->getId();
        $counter = Counter::where('ip', $ip)
                            ->where('session', $userSession)
                            ->first();
        if(!$counter){
            $counter = new Counter();
            $counter->ip = $ip;
            $counter->view_count = 1;
            $counter->user_agent = $request->header('user-agent');
            $counter->session = $userSession;
            $counter->save();
        }else{
            $counter->view_count = $counter->view_count + 1;
            $counter->user_agent = $request->header('user-agent');
            $counter->session = $userSession;
            $counter->save();
        }
        $totalView = Counter::sum("view_count");
        $expireAt = Carbon::now()->addHours(4);
        Cache::put('user-online-'.$ip.$userSession, true, $expireAt);
        $userOnline = Counter::all();
        $online = 0;
        foreach($userOnline as $user){
            if($user->isOnline()){
                $online = $online+1;
            }
        }
        return view('users.home')->with(
            [
                "link"      => $link, 
                'main'      => $main, 
                'settings'  => $settings, 
                'left'      => $left, 
                'ip'        => $ip,
                'totalView' => $totalView,
                'online'    => $online
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->type=='getIcon')
        {
            return $this->addLink($request);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $link = Link::whereId($id)->first();
        $link->name = $request->name;
        $link->url = $request->url;
        $link->save();
        return $link->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!empty(Auth::user())){
            if ($request->session()->has(Auth::user()->id.'_'.Auth::user()->firstname)) {
                $linkDeleted = $request->session()->get(Auth::user()->id.'_'.Auth::user()->firstname);
                $linkDeleted->push($id);
                $request->session()->put(Auth::user()->id.'_'.Auth::user()->firstname, $linkDeleted);
            }else{
                $request->session()->put(Auth::user()->id.'_'.Auth::user()->firstname, collect([$id]));
            }
            // if(Auth::user()->user_type != 3){
            $link = Link::whereId($id)->whereUserId(Auth::user()->id)->first();
            if(!empty($link))
            {
                $link->delete();
            }
            // }           
        }        
        // return "true";
        
    }

    public function addLink(Request $request){
        $formValidate = [
            'name'     =>  'required|string|max:255',
            'url'      =>  'required|string|max:350',
        ];
        $this->validate($request, $formValidate);
        $link = new Link;
        $link->name = $request->name;
        $link->url = $request->url;
        $link->category_id = 12;
        $link->enable = 1;
        $link->is_follow = 0;
        $link->user_id = Auth::user()->id;
        $link->save();
        return $link->id;
        // if ($request->session()->has(Auth::user()->id.'_'.Auth::user()->firstname)) {
        //     $linkSession = $request->session()->get(Auth::user()->id.'_'.Auth::user()->firstname);
        //     $linkSession = $linkSession->merge([$link]);
        //     $request->session()->put(Auth::user()->id.'_'.Auth::user()->firstname, $linkSession);
        // }else{
        //     $linkSession = collect([$link]);
        //     $request->session()->put(Auth::user()->id.'_'.Auth::user()->firstname, $linkSession);
        // }
        // return "true";

        // $limitTime = time() + 60 * 60 * 24 * 365; // one year
        // $userLink = $request->cookie(Auth::user()->id.'_'.Auth::user()->firstname);
        // $userLink = $userLink->toArray();
        // if(empty($userLink)){
        //     $userLink = $link;
        //     echo 0;
        // }
        // if(count($userLink) < 10){
        //     array_push($userLink, $link);
        //     echo 1;
        // }
        // return response("user_link")->cookie(
        //     Auth::user()->id.'_'.Auth::user()->firstname, $userLink, $limitTime
        // );
    }

    public function signup(Request $request)
    {
        if(!Auth::check()){
            return view('users.singup');
        }else{
            return redirect('/');
        }
    }

    public function updateLinkPosition(Request $request)
    {
        $currentUser = Auth::user();
        if($currentUser){
            $links = json_decode($request->data);
            $numOfLink = count($links->list);
            if($numOfLink > 20) {
                return;
            }
            $currentUser->limit_links = $numOfLink;
            $currentUser->save();
            foreach($links->list as $linkItem){
                $link = Link::where("id", $linkItem->id)->first();
                $link->priority = $numOfLink;
                $numOfLink--;
                $link->save();
            }
        }
    }
}
