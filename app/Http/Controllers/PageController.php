<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Traits\ListViewTrait;
use App\Models\Setting;
use App\Models\Counter;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{
    use ListViewTrait;
    
    protected $formValidate = [
        'name'          =>  'required|string|max:255',
        'description'   =>  'required',
        'status'        =>  'required',
    ];
    protected $model = Page::class;

    protected $header = [
        'id'                    =>  'ID',
        'name'                  =>  'Tên',
        'description'           =>  'Nội dung',
        'status_name'           =>  'Tình trạng'
    ];

    public function afterInsert($rawInput, $instance) 
    {
        $id = $instance->id;
        $urlSlug = str_slug($instance->name);
        $instance->url_slug = $urlSlug;
        $instance->save();
    }

    public function Post($slug)
    {
        $ip='';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $page = Page::where('url_slug', $slug)->first();
        if(!$page){
            return redirect("/404.html");
        }
        $settings = Setting::all();
        $counter = Counter::where('ip', $ip)->first();
        if(!$counter){
            $counter = new Counter();
            $counter->ip = $ip;
            $counter->view_count = 1;
            $counter->save();
        }else{
            $counter->view_count = $counter->view_count + 1;
            $counter->save();
        }
        $totalView = Counter::sum("view_count");
        
        $expireAt = Carbon::now()->addMinutes(5);
        Cache::put('user-online-'.$ip, true, $expireAt);
        
        $userOnline = Counter::all();
        $online = 0;
        foreach($userOnline as $user){
            if($user->isOnline()){
                $online = $online+1;
            }
        }
        if($slug == 'dang-ky-thanh-cong'){
            return view('auth.signupSuccess')->with([
                'page' => $page,
            ]);
        }
        return view('users.page')->with([
            'settings' => $settings,
            'ip'=>$ip, 
            'page' => $page,
            'totalView' => $totalView,
            'online'    => $online
        ]);
    }
}
