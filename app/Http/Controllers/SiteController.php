<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Traits\ListViewTrait;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class SiteController extends Controller
{
    use ListViewTrait;
    
    protected $formValidate = [
        'name'          =>  'required|string|max:255',
        'link'          =>  'required',
        'limit_record'  =>  'required|integer',
        'limit_show'    =>  'required|integer',
        'enable'        =>  'required',
    ];
    protected $model = Site::class;

    protected $header = [
        'id'                    =>  'ID',
        'name'                  =>  'Tên',
        'link'                  =>  'URL',
        'limit_record'          =>  'Giới hạn lấy tin',
        'limit_show'            =>  'Giới hạn hiển thị',
        'status_name'           =>  'Tình trạng'
    ];
}
