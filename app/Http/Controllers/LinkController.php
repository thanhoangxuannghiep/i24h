<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use App\Traits\ListViewTrait;
use Mockery\Tests\React_ReadableStreamInterface;

class LinkController extends Controller
{
    use ListViewTrait;
    protected $formValidate = [
        'name'     =>  'required|string|max:255',
        'url'      =>  'required|string|max:350',
        // 'logo'     =>  'required',
        'category_id'     =>  'required',
    ];
    protected $model = Link::class;
    protected $header = [
        'id'                    =>  'ID',
        'name'                  =>  'Tên',
        'url'                   =>  'Url',
        'logo_url'              =>  'Logo',
        'is_follow_name'        =>  'Follow',
        'category_name'         =>  'Danh mục',
        'enable_name'           =>  'Tình trạng',
        'email'                 =>  'Email',
        'priority'              =>  'Độ ưu tiên',
        'is_highlight_name'     =>  'Nổi bật'
    ];

    public function index(Request $request)
    {
        if (isset($this->filter)) {
            $modelData = $this->model::where($this->filter)->with('user')->get();
        } else {
            $modelData = $this->model::orderBy('id', 'desc')->with('user')->get();
        }
        $data = [];
        $header = array_values($this->header);
        switch ($request->type) {
            case 'admin':
                $newData = $modelData->filter(function($item){
                    $user = $item->user;
                    if($user !== NULL && $user['user_type'] == 3){
                        $item['email'] = $user->email;
                        return $item;
                    }
                }); 
                break;
            default:
                $newData = $modelData->filter(function($item){
                    $user = $item->user;
                    if($user !== NULL) $item['email'] = $user->email;
                    if($user === NULL ||
                        $user['user_type'] == 1 || 
                        $user['user_type'] === NULL){
                        return $item;
                    }
                });               
                break;
        }
        foreach ($newData as $record) {
            $data[] = $record->list(array_keys($this->header));
        }
        return [
            'header'    =>  $header,
            'data'      =>  $data
        ];
    }

    public function getList(Request $request){
        $links = Link::where('enable', 1)->orderBy('priority', 'desc')->limit(10)->get();
        if(!empty($request->idCat)){
            $linkTemp = Link::where("id", $request->id)->first();
            $links = Link::where('enable', 1)
                        ->where('category_id', $request->idCat)
                        ->where('user_id', $linkTemp->user_id)
                        ->orderBy('priority', 'desc')
                        ->get();
        }
        $return = [];
        foreach ($links as $item) {
            $return[] = [
                'id' => $item->id,
                'name' => $item->name,
                'order' => $item->priority
            ];
        }
        return $return;
    }

    public function beforeInsert(&$inputs)
    { 
        $listItems = $inputs['priority'];
        $temp = count($listItems);
        $priority = 0;
        foreach ($listItems as $key => $item) {
            if(!isset($item['id'])){
                $priority = $temp;
                continue;
            }
            $link = Link::where('id', $item['id'])->first();
            $link->priority = $temp;
            $link->save();
            $temp--;
        }
        $inputs['priority'] = $priority;
    }

    public function validateRequest($request)
    {
        $this->validate($request, $this->formValidate);
    }

    function imageName($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
