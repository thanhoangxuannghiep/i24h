<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Link;
use Illuminate\Support\Facades\Hash;
use App\Traits\ListViewTrait;

class UserController extends Controller
{
    use ListViewTrait;
    protected $formValidate = [
        'firstname'     =>  'required|string|max:255',
        'email'         => 'required|string|email|max:255',
        // 'password'      => 'required|string|min:6',
    ];
    protected $model = User::class;
    protected $header = [
        'id'                    =>  'ID',
        'firstname'             =>  'Tên',
        'email'                 =>  'Email',
        'user_type_name'        =>  'Loại user',
        'status_name'           =>  'Tình trạng',
        'limit_links'           =>  'Số lượng link'
    ];

    public function getListType()
    {
        return [
            [
                'id'    => '1',
                'name'  => "User"
            ],
            [
                'id'    => '3',
                'name'  => "Admin"
            ]
        ];
    }

    public function beforeInsert(&$inputs)
    { 
        $password = $inputs['password'];
        $user = User::where('id', $inputs['id'])->first();
        if($password == $user->password){
            unset($inputs['password']);
        }
        // else{
        //     echo $user->password.PHP_EOL;
        //     echo bcrypt($password);
        // }
        // if (password_verify(($password), $user->password)) {
            
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $flag = false;
        if($user->delete()){
            $flag = true;
            $userLink = Link::where('user_id', $id)->get();
            if(!empty($userLink)){
                foreach($userLink as $item){
                    $item->delete();
                }
            }
        }else{
            $flag = false;
        }
        return $flag ? "true" : "false";
    }

    public function validateRequest($request)
    {
        $this->validate($request, $this->formValidate);
    }
}
