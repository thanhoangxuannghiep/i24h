<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Box;
use App\Traits\ListViewTrait;

class BoxController extends Controller
{
    use ListViewTrait;

    protected $formValidate = [
        // 'name'      =>  'required|string|max:255|unique:categories',
        'name'      =>  'required|string|max:255',
        'position'  =>  'required',
        'priority'  =>  'required',
    ];
    protected $model = Box::class;
    protected $header = [
        'id'                    =>  'ID',
        'name'                  =>  'Tên',
        'position_name'         =>  'Vị trí',
        'priority'              =>  'Độ ưu tiên',
        'description'           =>  'Mô tả',
        'status_name'           =>  'Tình trạng'
    ];
    public function getPosition()
    {
        return [
            [
                'id'    => '1',
                'name'  => "Khu vực Top"
            ],
            [
                'id'    => '2',
                'name'  => "Khu vực Main"
            ],
            [
                'id'    => '3',
                'name'  => "Khu vực Left"
            ]
        ];
    }
    public function getList(Request $request)
    {
        $boxes = Box::where('status', 1)->orderBy('priority', 'desc')->get();
        if(!empty($request->idCat)){
            $boxes = Box::where('status', 1)
                        ->where('position', $request->idCat)
                        ->orderBy('priority', 'desc')
                        ->get();
        }
        $return = [];
        foreach ($boxes as $item) {
            $return[] = [
                'id' => $item->id,
                'name' => $item->name,
                'order' => $item->priority
            ];
        }
        return $return;
    }

    public function beforeInsert(&$inputs)
    { 
        $listItems = $inputs['priority'];
        $position = $inputs['position'];
        $temp = count($listItems);
        $priority = 0;
        foreach ($listItems as $key => $item) {
            if(!isset($item['order'])){
                $priority = $temp;
                continue;
            }
            $box = Box::where('id', $item['id'])->first();
            $box->priority = $temp;
            $box->save();
            $temp--;
        }
        $inputs['priority'] = $priority;
    }

    public function beforeUpdate(&$inputs)
    {        
        $listItems = $inputs['priority'];
        $temp = count($listItems);
        $priority = 0;
        foreach ($listItems as $key => $item) {
            $box = Box::where('id', $item['id'])->first();
            $box->priority = $temp;
            $box->save();
            if($inputs['id'] == $box->id){
                $priority = $temp;
            }
            $temp--;       
        }
        $inputs['priority'] = $priority;
    }
}
