<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class i24hResetPassword extends Notification
{
    use Queueable;
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('password.reset', $this->token.'?id='.$notifiable["attributes"]["id"], false);
        $url = trim($url, '/asim');
      // var_dump($notifiable);
        return (new MailMessage)
            ->subject('['.env('APP_NAME', 'i24h').'] Reset Your Password')
            ->greeting('Hello '. ucfirst($notifiable["attributes"]["firstname"]) .'!')
            ->line("We received a request to reset your password and we're here to help!")
            ->line('')
            ->action('Simply click here', url(config('app.url').'/'.$url))
            ->line('')
            ->line("If you didn't ask to change your password, don't worry! Your password is still safe and you can delete this email.")
            ->line("If you have any questions or concerns, feel free to reply directly to this email or contact us at help@i24h.vn")
            ->line("");
    }
}
