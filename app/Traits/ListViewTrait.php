<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait ListViewTrait
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($this->filter)) {
            $modelData = $this->model::where($this->filter)->get();
        } else {
            $modelData = $this->model::all();
        }

        $data = [];
        $header = array_values($this->header);
        foreach ($modelData as $record) {
            $data[] = $record->list(array_keys($this->header));
        }
        return [
            'header'    =>  $header,
            'data'      =>  array_values($data)
        ];
    }

    public function genHeader()
    {
        $selectstr = '';
        foreach ($this->header as $key => $value) {
            $selectstr .= sprintf('%s, ', $key);
        }
        return rtrim($selectstr, ', ');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        $rawInputs = $request->except('_token');
        $inputs = $rawInputs;

        $this->beforeInsert($inputs);
        $insert = $this->model::create($inputs);

        $insert->save();
        $this->afterInsert($rawInputs, $insert);

        return array_values($insert->list(array_keys($this->header)));
    }

    public function beforeInsert(&$inputs) { }
    public function afterInsert($rawInput, $instance) { }

    public function beforeDestroy($id) { }
    public function afterDestroy($id) { }

    public function beforeUpdate(&$inputs)
    {
        $this->beforeInsert($inputs);
    }
    public function afterUpdate($rawInput, $instance)
    {
        $this->afterInsert($rawInput, $instance);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        switch ($request->_method) {
            case 'update':
                return $this->update($request, $id);
                break;
            case 'delete':
                return $this->destroy($id);
                break;
            default:
                $record = $this->model::find($id);
                return $record;
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateRequest($request);
        $rawInputs = $request->except('_token');
        $inputs = $rawInputs;

        $this->beforeUpdate($inputs);
        $update = $this->model::updateOrCreate(['id' => $id], $inputs);

        $update->save();
        $this->afterUpdate($rawInputs, $update);

        return array_values($update->list(array_keys($this->header)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->beforeDestroy($id);
        $status = $this->model::where('id', $id)
            ->delete();
        $this->afterDestroy($id);
        return $status;
    }

    public function resources()
    {
        if (isset($this->filter)) {
            $modelData = $this->model::where($this->filter)->get();
        } else {
            $modelData = $this->model::all();
        }
        $result = [];
        foreach ($modelData as $record) {
            $result[] = $record->getResourceList();
        }
        return $result;
    }

    public function validateRequest($request)
    {
        $this->validate($request, $this->formValidate);
    }

    public function handleFileUpload($inputs, $instance)
    {
        foreach ($inputs as $key => $value) {
            if (! str_contains($value, [';base64'])) {
                if (! isset($instance->{$key}) || is_null($instance->{$key})) {
                    $instance->{$key} = null;
                }
                continue;
            }
            $instance->{$key} = $this->storeFile($value, $instance->id);
        }
        return $instance;
    }

    private function storeFile($value, $name)
    {
        $file_type = '';
        if (str_contains($value, ['data:image/'])) {
            $fileData = explode(';base64,', $value);
            $file_type = explode('data:image/', $fileData[0])[1];
            $file = \Image::make(base64_decode($fileData[1]))->fit(200);
            $file->save(public_path() . config('params.file_path.image') . $name . '.' .$file_type);
        }

        if (str_contains($value, ['data:application/'])) {
            $fileData = explode(';base64,', $value);
            $file_type = explode('data:application/', $fileData[0])[1];
            file_put_contents(public_path() . config('params.file_path.pdf') . $name . '.' . $file_type, base64_decode($fileData[1]));
        }
        return $name . '.' . $file_type;
    }

    public function getValueOfKey($key, $data)
    {
        $result = [];
        foreach ($data as $object) {
            $result[] = $object->$key;
        }
        return $result;
    }
}