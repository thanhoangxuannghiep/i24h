<?php 
namespace App\Events;

use App\Models\Counter;

class CountViewHandler
{
    public function handle(Counter $post)
    {
        $post->increment('view_count');
    }
}