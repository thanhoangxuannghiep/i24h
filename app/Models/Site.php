<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'sites';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'link', 
        'limit_record',
        'limit_show',
        'enable', 'status_name'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getStatusNameAttribute($value)
    {
        $enable = $this->enable ? $this->enable : 0;
        return config('params.status.'.$enable);
    }

    public function list($cols){
        $result = [];
        foreach ($cols as $col) {
            $result[] = $this->$col;
        }
        return $result;
    }
}
