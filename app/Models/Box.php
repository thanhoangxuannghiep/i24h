<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'position', 'priority', 'status', 'status_name'
    ];

    protected $appends = ['position_name'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getPositionNameAttribute($value)
    {
        return config('params.position.'.$this->position);
    }

    public function getStatusNameAttribute($value)
    {
        return config('params.status.'.$this->status);
    }
    
    public function links(){
        return $this->hasMany("App\Models\Link", 'category_id', 'id')
                    ->where('is_highlight', 0)
                    ->orderBy('priority', 'desc');
    }

    public function highlightLinks(){
        return $this->hasMany("App\Models\Link", 'category_id', 'id')
                    ->where('is_highlight', 1)
                    ->orderBy('priority', 'desc')->limit(5);
    }

    public function list($cols){
        $result = [];
        foreach ($cols as $col) {
            $result[] = $this->$col;
        }
        return $result;
    }

    public function getResourceList()
    {
        return [
            'id'    =>  $this->id,
            'name'  =>  $this->name
        ];
    }
}
