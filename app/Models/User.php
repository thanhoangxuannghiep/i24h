<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\i24hResetPassword as ResetPasswordNotification;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'user_type', 'status', 'limit_links'
    ];

    protected $appends = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'raw_password'
    ];
    public $rawPassword = '';

    public function setPasswordAttribute($value)
    {
        $this->rawPassword = $value;
        $this->attributes['password'] = bcrypt($value);
    }

    public function getUserTypeNameAttribute($value)
    {
        return config('params.user_type.'.$this->user_type);
    }

    public function getStatusNameAttribute($value)
    {
        if($this->status == false) $this->status = 0;
        return config('params.status.'.$this->status);
    }

    public function list($cols){
        $result = [];
        foreach ($cols as $col) {
            $result[] = $this->$col;
        }
        return $result;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    
}
