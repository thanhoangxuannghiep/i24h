<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'logo', 'logo_url', 
        'url', 
        'is_follow', 'is_follow_name', 
        'category_id', 'category_name', 
        'enable', 'enable_name', 
        'user_id', 
        'priority',
        'is_highlight', 'is_highlight_name'
    ];

    public function getEnableNameAttribute($value)
    {
        return config('params.status.'.$this->enable);
    }

    public function getIsFollowNameAttribute($value)
    {
        if(empty($this->is_follow)) $this->is_follow = 0;
        return config('params.follow.'.$this->is_follow);
    }

    public function getIsHighlightNameAttribute($value)
    {
        if(empty($this->is_highlight)) $this->is_highlight = 0;
        return config('params.highlight.'.$this->is_highlight);
    }

    public function getLogoUrlAttribute($value)
    {
        return "<img src='https://www.google.com/s2/favicons?domain=".$this->logo."' width='50px' height='50px'>";
    }

    public function getCategoryNameAttribute($value)
    {
        $cat = Box::where('id', $this->category_id)->first();
        if(empty($cat)) return '';
        return $cat->name;
    }
    // protected $appends = ['position_name'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function category(){
        return $this->hasOne("App\Models\Box", 'id', 'category_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function list($cols){
        $result = [];
        foreach ($cols as $col) {
            $result[] = $this->$col;
        }
        return $result;
    }
}
