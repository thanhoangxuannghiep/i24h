<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Counter extends Model
{
    protected $table = 'posts';
    protected $fillable = [ 'ip', 'view_count', 'user_agent' ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function isOnline()
    {
        return Cache::has('user-online-'.$this->ip.$this->session);
    }
}
