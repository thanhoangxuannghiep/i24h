<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'description', 
        'url_slug',
        'status', 'status_name'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getStatusNameAttribute($value)
    {
        return config('params.status.'.$this->status);
    }

    public function list($cols){
        $result = [];
        foreach ($cols as $col) {
            $result[] = $this->$col;
        }
        return $result;
    }
}
