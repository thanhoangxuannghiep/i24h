
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
require('../plugins/jquery/jquery-2.1.1');
require('./bootstrap.min');
Vue.use(require('v-toaster'), { timeout: 5000 });
require('./admin_routes');
require('../plugins/slimscroll/jquery.slimscroll.min');
require('../plugins/metisMenu/jquery.metisMenu');
require('../plugins/select2/select2.full.min');

require('../plugins/summernote/summernote.min');
require('../plugins/pace/pace.min');
require('./form_error');
require('./form');
require('./filter');
require('./window_route');
require('./elements');
require('./float_title');
require('../plugins/dataTables/datatables.min');
window.Vue = require('vue');
window.VueStrap = require('vue-strap');
window.debug_ = 0;
window.DataTables = require('datatables.net');

Vue.component('alert', VueStrap.alert);
Vue.component('modal', VueStrap.modal);
Vue.component('admin-sidebar', require('./components/adminSidebar.vue'));
Vue.component('file-upload', require('./components/FileUpload'));
Vue.component('custom-table', require('./components/CustomTable'));
Vue.component('admin-breadcum', require('./components/AdminBreadcum'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.usersShared = {
    users: {},
    user:{}
};
window.sharedParams_ = {

};
window.loggedUser = currentUserId;

const admin = new Vue({
    el: '#wrapper',
    router: router,
    props: [

    ],
    data() {
        return {
            logout: new Form_(),
            shared_: sharedParams_,
            headerInfo: {},
            loggedUser: currentUserId
        }
    },
    methods: {

    },
    mounted() {

    },
    created() {
    },
    updated() {
        // window.TSC_.init();
    }
});