<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'i24h') }}</title>
    <link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />
    <!-- Styles -->
    <link href="{{ mix('css/app.css') . '?t=' . microtime() }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') . '?t=' . microtime() }}"></script>
</head>
<body>
<div id="wrapper">
    <div class="list-group">
        @foreach($repos as $repo)
            <a class="list-group-item" href="/finder?repo={{ $repo['name'] }}">
                <h4 class="list-group-item-heading">{{ $repo['name'] }}</h4>
                <p class="list-group-item-text">{{ $repo['description'] }}</p>
            </a>
        @endforeach
    </div>
</div>
</body>
</html>