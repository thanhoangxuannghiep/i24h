<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <title>{{ config('app.name') }}</title>
    <link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />

    <!-- Styles -->
    <link href="{{ mix('css/basic.css') }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/basic.js') . '?t=' . microtime() }}"></script>
</body>
</html>
