<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'i24h admin') }}</title>
    <link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />
    <!-- Styles -->
    <link href="{{ mix('css/admin.css') . '?t=' . microtime() }}" rel="stylesheet">
    <script>
        var currentUserId = {!! Auth::user() ?: "" !!};
    </script>
</head>
<body>
<div id="wrapper">
{{--    @include('partials.admin_navigation')--}}
    <admin-sidebar></admin-sidebar>
    <div id="page-wrapper" class="gray-bg">
        @include('partials.admin_topnavbar')
        @yield('content')
        @include('partials.admin_footer')
    </div>
</div>
<script src="/js/pace.full.min.js"></script>
<script src="{{ mix('js/admin.js') . '?t=' . microtime() }}"></script>
@yield('custom-js')
</body>
</html>