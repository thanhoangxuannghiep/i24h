<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<?php
    // $protocol = "http://";
    // if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)
    // {
    //     $protocol = "https://";
    // }
    // $shortText = "[InternetShortcut]\nURL={$protocol}" . $_SERVER['SERVER_NAME'];
 
    // // We'll be outputting an internet shortcut file
    // header('Content-type: application/internet-shortcut');
 
    // // It will be called myShortCut.URL
    // header('Content-Disposition: attachment; filename="myShortCut.URL"');
 
    // //output URL file
    // echo $shortText;
 
?> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    @php
        $meta = '';
        $filtered = $settings->filter(function ($value, $key) use(&$meta) {
            if(preg_match('/meta/',$value->path)){
                $meta = $value->value;
            }
        });
        echo $meta;
    @endphp
    <title>{{ config('app.name', 'i24h') }}</title>
    <link rel='shortcut icon' href="{{ asset('images/favicon.ico') }}?v=2" />
    <!-- Styles -->
    <link href="{{ mix('css/app.css') . '?t=' . microtime() }}" rel="stylesheet">
    <link href="{{ 'css/theme.css' . '?t=' . microtime() }}" rel="stylesheet">
    <link href="{{ 'css/bootstrap.css' . '?t=' . microtime() }}" rel="stylesheet">
    <link rel="icon" sizes="192×192" href="{{ asset('images/favicon.ico') }}">
    <link href="{{ 'css/custom.css' . '?t=' . microtime() }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') . '?t=' . microtime() }}"></script> 
    <script>
        var currentUserId = '{!! Auth::user() ?: "" !!}'
    </script>
    <script type="text/javascript">
        var Liferay = {
            Browser: {
                acceptsGzip: function() {
                    return true;
                },
                getMajorVersion: function() {
                    return 56.0;
                },
                getRevision: function() {
                    return "537.36";
                },
                getVersion: function() {
                    return "56.0.2924.87";
                },
                isAir: function() {
                    return false;
                },
                isChrome: function() {
                    return true;
                },
                isFirefox: function() {
                    return false;
                },
                isGecko: function() {
                    return true;
                },
                isIe: function() {
                    return false;
                },
                isIphone: function() {
                    return false;
                },
                isLinux: function() {
                    return true;
                },
                isMac: function() {
                    return false;
                },
                isMobile: function() {
                    return false;
                },
                isMozilla: function() {
                    return false;
                },
                isOpera: function() {
                    return false;
                },
                isRtf: function() {
                    return true;
                },
                isSafari: function() {
                    return true;
                },
                isSun: function() {
                    return false;
                },
                isWap: function() {
                    return false;
                },
                isWapXhtml: function() {
                    return false;
                },
                isWebKit: function() {
                    return true;
                },
                isWindows: function() {
                    return false;
                },
                isWml: function() {
                    return false;
                }
            },

            Data: {
                isCustomizationView: function() {
                    return false;
                },

                notices: [
                null
                ]
            },

            ThemeDisplay: {
                getCompanyId: function() {
                    return "18301";
                },
                getCompanyGroupId: function() {
                    return "18338";
                },
                getUserId: function() {
                    return "18305";
                },
                getDoAsUserIdEncoded: function() {
                    return "";
                },
                getPlid: function() {
                    return "18330";
                },

                getLayoutId: function() {
                    return "1";
                },
                getLayoutURL: function() {
                    return "http://{{env('SESSION_DOMAIN', 'i24h.vn')}}";
                },
                isPrivateLayout: function() {
                    return "false";
                },
                getParentLayoutId: function() {
                    return "0";
                },


                getScopeGroupId: function() {
                    return "18327";
                },
                getScopeGroupIdOrLiveGroupId: function() {
                    return "18327";
                },
                getParentGroupId: function() {
                    return "18327";
                },
                isImpersonated: function() {
                    return false;
                },
                isSignedIn: function() {
                    return false;
                },
                getDefaultLanguageId: function() {
                    return "en_US";
                },
                getLanguageId: function() {
                    var strTemp = "en_US";
                    var lanId = strTemp.replace(/\<|\>|\"|\'|\%|\;|\(|\)|\&|\+|\-/g, "");

                    return lanId;
                },
                isAddSessionIdToURL: function() {
                    return false;
                },
                isFreeformLayout: function() {
                    return false;
                },
                isStateExclusive: function() {
                    return false;
                },
                isStateMaximized: function() {
                    return false;
                },
                isStatePopUp: function() {
                    return false;
                },
                getPathContext: function() {
                    return "";
                },
                getPathImage: function() {
                    return "/image";
                },
                getPathJavaScript: function() {
                    return "/html/js";
                },
                getPathMain: function() {
                    return "/c";
                },
                getPathThemeImages: function() {
                    return "/assets/images";
                },
                getPathThemeRoot: function() {
                    return "/assets/";
                },
                getURLHome: function() {
                    return "http://{{env('SESSION_DOMAIN', 'i24h.vn')}}";
                },
                getSessionId: function() {
                    return "278387E5E5377FAFE128CFF5D424FA9F";
                },
                getPortletSetupShowBordersDefault: function() {
                    return true;
                }
            },

            PropsValues: {
                NTLM_AUTH_ENABLED: false
            }
        };

        var themeDisplay = Liferay.ThemeDisplay;



        Liferay.AUI = {
            getBaseURL: function() {
                return 'http://{{env("SESSION_DOMAIN", "i24h.vn")}}/html/js/aui/';
            },
            getCombine: function() {
                return true;
            },
            getComboPath: function() {
                return '/combo/?browserId=other&minifierType=&languageId=en_US&b=6100&t=1488729265000&p=/html/js&';
            },
            getFilter: function() {


                return {
                    replaceStr: function(match, fragment, string) {
                        return fragment + 'm=' + (match.split('/html/js')[1] || '');
                    },
                    searchExp: '(\\?|&)/([^&]+)'
                };




            },
            getJavaScriptRootPath: function() {
                return '/html/js';
            },
            getLangPath: function() {
                return null;
            },
            getRootPath: function() {
                return '/html/js/aui/';
            }
        };

        window.YUI_config = {
            base: Liferay.AUI.getBaseURL(),
            comboBase: Liferay.AUI.getComboPath(),
            fetchCSS: true,
            filter: Liferay.AUI.getFilter(),
            root: Liferay.AUI.getRootPath(),
            useBrowserConsole: false
        };
        Liferay.currentURL = '\x2f\x3fhvn\x3dd0e91d37f2';
        Liferay.currentURLEncoded = '%2F%3Fhvn%3Dd0e91d37f2';
    </script>
    <script src="/js/barebone.jsp" type="text/javascript"></script>
    <script src="/js/main.js"></script>
    <script src="/js/mainDesktop.js"></script>
    <script src="/js/pluginBlock.js"></script>
    <script src="/js/jquery-cookie.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-min-1.10.1.js" type="text/javascript"></script>
    <script src="/js/jquery.contextMenu2.js" type="text/javascript"></script>
    <script src="/js/tooltip.js" type="text/javascript"></script>
    <script src="/js/jquery.ui.autocomplete.html.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $('.errAddLink').hide();
            $('.errLogin').hide();
        })
        $('.searchGoogle').click(function(){
            var key = $('.searchKey').val();
            window.location.href = "https://www.google.com.vn/search?q="+key.replace(" ", "+");
        });
        $('.addNewLink').click(function(){
            var currentUserId = '{!! Auth::check() ? Auth::user() : 'none' !!}';
            if(currentUserId == "none"){
                $(".errLogin").hide();
                $("#modal-form").modal("show");
            }else{
                $(".addLink").modal("show");
            }
        });
        $(document).on('click', '.btnLogin', function(){
            $.ajax({
                type: "POST",
                url: '/login',
                cache: true,
                async: true,
                data: {
                    email: $("input[type='email']").val(), 
                    password: $("input[type='password']").val() ,
                    _token: "{{csrf_token()}}"
                },
                success: function(data) {
                    location.reload();
                },
                error: function(jqXHR, exception) {
                    $(".errLogin").html(jqXHR.responseJSON.email);
                    $(".errLogin").html(jqXHR.responseJSON.password);
                    $(".errLogin").show();
                }
            });
        });
        $(document).on('click', '.btnAddLink', function(){
            $.ajax({
                type: "POST",
                url: '/addLink',
                cache: true,
                async: true,
                data: {
                    name: $("input[name='name']").val(), 
                    url: $("input[type='url']").val(),
                    _token: "{{csrf_token()}}"
                },
                success: function(data) {
                    location.reload();
                },
                error: function(jqXHR, exception) {
                    if(jqXHR.responseJSON.name){
                        $(".errAddLink").html(jqXHR.responseJSON.name);    
                    }
                    if(jqXHR.responseJSON.url){
                        $(".errAddLink").html(jqXHR.responseJSON.url);    
                    }
                    $(".errAddLink").show();
                }
            });
        });
    </script>
    <script type="text/javascript">
        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_103_',
            portletId: '103',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3d103\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3d\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d0\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 1,
            isStatic: 'end',
            namespacedId: 'p_p_id_56_INSTANCE_eMk7Vqyyi7sa_',
            portletId: '56_INSTANCE_eMk7Vqyyi7sa',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3d56_INSTANCE_eMk7Vqyyi7sa\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-2\x26p_p_col_pos\x3d1\x26p_p_col_count\x3d3\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_1_WAR_weblinksportlet_',
            portletId: '1_WAR_weblinksportlet',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3d1_WAR_weblinksportlet\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-1\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d1\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_2_WAR_weblinksportlet_INSTANCE_5D4vZlmTEAYa_',
            portletId: '2_WAR_weblinksportlet_INSTANCE_5D4vZlmTEAYa',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3d2_WAR_weblinksportlet_INSTANCE_5D4vZlmTEAYa\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-4\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d1\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_Extensions_WAR_Extensionsportlet_',
            portletId: 'Extensions_WAR_Extensionsportlet',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dExtensions_WAR_Extensionsportlet\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-3\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d2\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 2,
            isStatic: 'end',
            namespacedId: 'p_p_id_film_WAR_Utilitiesportlet_',
            portletId: 'film_WAR_Utilitiesportlet',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dfilm_WAR_Utilitiesportlet\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-2\x26p_p_col_pos\x3d2\x26p_p_col_count\x3d3\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_newsviewaction_WAR_weblinksportlet_',
            portletId: 'newsviewaction_WAR_weblinksportlet',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dnewsviewaction_WAR_weblinksportlet\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-2\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d3\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 1,
            isStatic: 'end',
            namespacedId: 'p_p_id_seo_WAR_BkavToolsportlet_',
            portletId: 'seo_WAR_BkavToolsportlet',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dseo_WAR_BkavToolsportlet\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-3\x26p_p_col_pos\x3d1\x26p_p_col_count\x3d2\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_loginpopup_WAR_weblinksportlet_INSTANCE_E3j7_',
            portletId: 'loginpopup_WAR_weblinksportlet_INSTANCE_E3j7',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dloginpopup_WAR_weblinksportlet_INSTANCE_E3j7\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3d\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d0\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });

        Liferay.Portlet.onLoad({
            canEditTitle: false,
            columnPos: 0,
            isStatic: 'end',
            namespacedId: 'p_p_id_weatherv3_WAR_weatherv3portlet_INSTANCE_h3y5_',
            portletId: 'weatherv3_WAR_weatherv3portlet_INSTANCE_h3y5',
            refreshURL: '\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d18330\x26p_p_id\x3dweatherv3_WAR_weatherv3portlet_INSTANCE_h3y5\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3d\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d0\x26p_p_isolated\x3d1\x26currentURL\x3d\x252F\x253Fhvn\x253Dd0e91d37f2'
        });
        YUI().use('aui-base', 'liferay-menu', 'liferay-notice', 'liferay-poller', function(A) {
            (function() {
                Liferay.Util.addInputType();

                Liferay.Portlet.ready(
                    function(portletId, node) {
                        Liferay.Util.addInputType(node);
                    }
                    );
            })();
            (function() {
                new Liferay.Menu();

                var liferayNotices = Liferay.Data.notices;

                for (var i = 1; i < liferayNotices.length; i++) {
                    new Liferay.Notice(liferayNotices[i]);
                }
            })();
        });

        function saveData(strUrl) {
            var items = $('#sortable').children();

            var itemsData = [];

            if (items != null && items.length > 0) {

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];

                    var url = $(item).find('a').attr('href');

                    var name = $(item).find('img').attr('alt');

                    var itemData = {
                        "id": i,
                        "url": url,
                        "name": name
                    };

                    itemsData.push(itemData);
                }

                var JsonData = {
                    "list": itemsData
                };

                jQuery.ajax({
                    type: "POST",

                    url: strUrl,

                    dataType: 'json',

                    contenttype: 'application/json; charset=utf-8',

                    data: {
                        data: JSON.stringify(JsonData),
                    },

                    success: function(response) {


                        jQuery.blockUI({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });

                        setTimeout(jQuery.unblockUI, 3000);

                    },

                    error: function(xhr, textStatus, errorThrown) {}

                });
            }
        }
    </script>
    
</head>
<body class="yui3-skin-sam controls-visible guest-site signed-out public-page site">
<div id="wrapper">
    <div id="p_p_id_loginpopup_WAR_weblinksportlet_INSTANCE_E3j7_" class="portlet-boundary portlet-boundary_loginpopup_WAR_weblinksportlet_  portlet-static portlet-static-end portlet-borderless loginpopup-portlet ">
        <span id="p_loginpopup_WAR_weblinksportlet_INSTANCE_E3j7"></span>
        <div class="portlet-body">
            <script type="text/javascript">
                function getItemData() {
                    var items = $('#sortable').children();

                    var itemsData = [];

                    if (items != null && items.length > 0) {

                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];

                            var url = $(item).find('a').attr('href');

                            var name = $(item).find('img').attr('alt');

                            var itemData = {
                                "id": i,
                                "url": url,
                                "name": name
                            };

                            itemsData.push(itemData);
                        }
                        return JSON.stringify({
                            "list": itemsData
                        });
                    }
                    return;
                }
            </script>
        </div>
    </div>
    <div id="skin-bgimage"></div>
    <div id="hdsd" class="hover-tooltip">
    </div>
    <div id="tooltip-film">
        <div class="left"></div>
        <div class="right"></div>
    </div>
    <div id="site-content">
        <div id="idBannerHome">
        </div>
        <div id="wrapper">
            @include('partials.user_topnavbar')
            <!-- <div class="row"> -->
                <!-- <div class="col-md-12"> -->
                    @yield('content')
                <!-- </div> -->
            <!-- </div> -->
            <div class="footer">
                <div class="col-xs-2"  style="padding:0;">
                    @php
                        $social = [];
                        $filtered = $settings->filter(function ($value, $key) use(&$social){
                            if(preg_match('/link/',$value->path)){
                                $social[$value->path] = $value->value;
                            }
                        });
                    @endphp
                    <a href="{{$social ? $social['linkFacebook'] : ''}}" class="fa-icon" target="_blank">
                        <i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;
                    </a>
                    <a href="{{$social ? $social['linkTweeter'] : ''}}" class="fa-icon" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;
                    </a>
                    <a href="{{$social ? $social['linkGoogle'] : ''}}" class="fa-icon" target="_blank">
                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-xs-6" style="padding:0;">
                    <div class="col-md-4">CodeXanh &copy; 2018 &nbsp; | &nbsp; </div>
                    <div class="col-md-4"><a href="/huong-dan-su-dung.html" target="_blank">Hướng dẫn sử dụng</a> &nbsp; | &nbsp;</div>
                    <div class="col-md-4"><a href="/lien-he.html" target="_blank">Liên hệ</a></div>
                </div>
                <div class="col-xs-4" style="text-align:right;">
                    
                    <div class="col-md-4">Total: {{$totalView}}</div>
                    <div class="col-md-4">Online: {{$online}} &nbsp;|&nbsp; </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<script type="text/javascript">
    function getHostName(url) {
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            return match[2];
        } else {
            var match2 = url.match(/(www[0-9]?\.)?(.[^/:]+)/i);
            if (match2 != null && match2.length > 2 && typeof match2[2] === 'string' && match2[2].length > 0) {
                return match2[2];
            } else {
                return null;
            }
        }
    }

    function checkSiteExits() {
        var checkResult = false;
        var items = jQuery('#sortable').children();
        if (items != null && items.length > 0 && !isEdit) {
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemUrl = jQuery(item).find("a").attr("href");
                var addUrl = $('#_1_WAR_weblinksportlet_siteUrl').val();
                if (getHostName(itemUrl) == getHostName(addUrl)) {
                    checkResult = true;
                    break;
                }
            }
        }

        return checkResult;
    }

    function btnAcceptClick() {
        if (checkSiteExits()) {
            if (confirm('Website đã có, bạn có muốn thêm tiếp?')) {
                addSiteInfo();
            }
        } else {
            addSiteInfo();
        }
        isEdit = false;
        manualSelectIcon = false;
    }
</script>
<script src="/js/pace.full.min.js"></script>
<!-- <script type="text/javascript" src="{{ URL::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script> -->
</body>
</html>
