<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img alt="i24h" class="logo" src='{{env("APP_LOGO")}}' data-holder-rendered="true" >
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-navbar-collapse">
            <form class="navbar-form search navbar-left">
                <span class="fa fa-search searchGoogle"></span>
                <input type="text" class="form-control searchKey" id="gs_input" placeholder="Tìm Kiếm trên Google">
            </form>
            <div id="amlich-text"></div>
            <div id="amlich-month"></div>
            <script type="text/javascript">
                $(function() {
                    $('#amlich-text').amLich({
                    type: 'text'
                    });

                    $('#amlich-month').amLich({
                        type: 'month',
                        tableWidth: '200px'
                    });
                });
            </script>
            <ul class="nav navbar-nav navbar-right">
                <li class="ip">
                    <span>IP: </span>
                    <span>{{$ip}}</span>
                </li>
                @if(!Auth::check())
                <li>
                    <a data-toggle="modal" href="#modal-form">Đăng Nhập</a>
                    |
                    <a href="/signup">Đăng Ký</a>
                </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user" aria-hidden="true" style="font-size: 1.5em; color:#137cbf;"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;">Hi {{Auth::user()->firstname}} {{Auth::user()->lastname}}</a></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </li>
                @endif
                
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
<div id="modal-form" class="modal modalLogin fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Đăng nhập</h3>   
                        <form role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="errLogin"></div>
                            <div class="form-group"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control"></div>
                            <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div>
                            <div>
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs btnLogin" type="button"><strong>Đăng nhập</strong></button>
                                <a href="/password/reset" target="_blank"> Quên mật khẩu? </a>
                                <label><input type="checkbox" name="remember"> Ghi nhớ mật khẩu </label>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6 signupTitle"><h4>Chưa là thành viên?</h4>
                        <p>Đăng ký ngay:</p>
                        <p class="text-center">
                            <a href="/signup"><i class="fa fa-sign-in big-icon"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var suggestCallBack;

    jQuery(document).ready(function() {
        var api = 'http://suggestqueries.google.com/complete/search?callback=?';
        var formAction = 'https://www.google.com/search';
        var search_type = jQuery('.search_option-selected').attr('id');
        jQuery("#gs_input").keypress(function(e){
            if (e.keyCode == 13) {
                var str = jQuery("#gs_input").val();
                var res = str.replace(/ /g, "+");
                window.open(
                    "https://www.google.com/search?q=" + res,
                    '_blank'
                );
            }
        });
        jQuery("#gs_input").autocomplete({
            source: function(request, response) {
                jQuery.getJSON(api, {
                    hl: 'vi',
                    jsonp: 'suggestCallBack',
                    q: request.term,
                    client: 'hp'
                });
                suggestCallBack = function(data) {

                    var suggestions = [];

                    jQuery.each(data[1], function(key, val) {

                        if (val != 'undefined') {

                            var valTemp = val[0];

                            var value = jQuery('<em>' + val[0] + '</em>').text();

                            suggestions.push({
                                "label": val[0],
                                "value": value
                            });
                        }

                    });

                    if (suggestions != '' && suggestions.length > 10) {
                        suggestions.length = 10;
                    }

                    response(suggestions);

                };
            },
            html: 'html',
            messages: {
                noResults: '',
                results: function() {}
            },
            select: function(event, ui) {
                event.preventDefault();
                var selectedObj = ui.item;
                jQuery('#gs_input').val(selectedObj.value);
                if (formAction == 'https://www.google.com/search') {
                    if (search_type == 'image_search') {
                        var str = jQuery("#gs_input").val();
                        var res = str.replace(/ /g, "+");
                        window.open(
                            "https://www.google.com/search?q=" + res + "&tbm=isch",
                            '_blank'
                            );
                    } else if (search_type == 'video_search') {
                        var str = jQuery("#gs_input").val();
                        var res = str.replace(/ /g, "+");
                        window.open(
                            "https://www.google.com/search?q=" + res + "&tbm=vid",
                            '_blank'
                            );
                    } else {
                        var str = jQuery("#gs_input").val();
                        var res = str.replace(/ /g, "+");
                        window.open(
                            "https://www.google.com/search?q=" + res,
                            '_blank'
                        );
                        // jQuery('#gs_f').submit();
                    }
                } else {
                    var str = jQuery("#gs_input").val();
                    var res = str.replace(/ /g, "+");
                    window.open(
                        "https://www.google.com/search?q=" + res,
                        '_blank'
                    );
                }
            }
        });
    });

    function loadScript(url, callback) {

        var script = document.createElement("script")
        script.type = "text/javascript";

            if (script.readyState) { //IE
                script.onreadystatechange = function() {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { //Others
                script.onload = function() {
                    callback();
                };
            }

            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        }
    </script>
<div class="modal fade addLink" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 b-r"><h3 class="m-t-none m-b">Thêm mới link</h3>   
                        <form role="form">
                            <div class="errAddLink"></div>
                            <div class="form-group"><label>Url</label> <input type="url" placeholder="Enter url" class="form-control"></div>
                            <div class="form-group"><label>Tên</label> <input name="name" type="text" placeholder="Tên" class="form-control"></div>
                            <div>
                                <button class="btn btn-primary pull-right btnAddLink" type="button"><strong>Đồng ý</strong></button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Bỏ qua</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</nav>