@extends('layouts.user')

@section('content')
<div class="columns-1-2" id="main-content" role="main">
    <div class="portlet-layout">
        <div class="portlet-column portlet-column-only" id="column-1">
            <div class="portlet-dropzone portlet-column-content portlet-column-content-only" id="layout-column_column-1">
                <div id="p_p_id_1_WAR_weblinksportlet_" class="portlet-boundary portlet-boundary_1_WAR_weblinksportlet_  portlet-static portlet-static-end portlet-borderless weblinks client-portlet ">
                    <span id="p_1_WAR_weblinksportlet"></span>
                    <div class="portlet-body">
                        <div class="portlet-borderless-container">
                            <div class="portlet-body">
                                <input type="hidden" id="checkEditAdd" value="">
                                <div class="ui-bg color-block-ie">
                                    <div id="sortable-weblink">
                                        <ul id="sortable" class="ui-sortable">
                                            @foreach ($link as $item)
                                            <li class="ui-state-default-first" id="0" rel="{{$item->id}}">
                                                <span style="display: inline-block;" class="item-content">
                                                    @php
                                                        $follow = ($item->is_follow == 0 || empty($item->is_follow)) ? 'nofollow' : 'follow';
                                                    @endphp
                                                    <a title="{{$item->name}}" href="{{$item->url}}" rel="{{$follow}}" class="item-href" target="_blank" rel="{{$item->name}}">
                                                        @php
                                                            $temp = parse_url($item->url);
                                                            $domain = $temp['host'];
                                                        @endphp
                                                        <img alt="{{$item->name}}" src="https://www.google.com/s2/favicons?domain={{$item->url}}" height ="25px" width="25px"/>
                                                    
                                                        <p class="item-name">{{$item->name}}</p>
                                                    </a>
                                                </span>
                                                <span class="edit-item" onclick="editSite(this);">
                                                    <i class="fa fa-edit"></i>
                                                </span>
                                                <span class="remove-item" onclick="removeSite(this);">
                                                    <i class="fa fa-remove"></i>
                                                </span>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <img id="image-hdsd" alt="huong dan su dung i24h.vn" src="/images/Tooltip-ok.png">
                                        <input type="hidden" id="check-show-hdsd" value="0">
                                        <div id="showBoxpromptdiv" style="float: left; width: 70px; height: 70px;">
                                            <div id="showBoxprompt" class="show_box-prompt" title="Thêm mới website"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="editBoxPromptDialog" class="edit-box-prompt">
                                    <div class="box-prompt-header"> <span id="boxPromptDialogHeaderTitle"></span> </div>
                                    <div class="box-prompt-content">
                                        <div class="aui-layout">
                                            <div class="aui-layout-content">
                                                <div class="aui-column ">
                                                    <div class="aui-column-content "> 
                                                        <span class="label_box-prompt" style="margin-top: 3px"> Địa chỉ website </span> 
                                                    </div>
                                                </div>
                                                <div class="aui-column ">
                                                    <div class="aui-column-content">
                                                        <span class="aui-field aui-field-text input_box-prompt"> 
                                                            <span class="aui-field-content">
                                                                <span class="aui-field-element "> 
                                                                    <input class="aui-field-input aui-field-input-text" id="_1_WAR_weblinksportlet_siteUrl" name="_1_WAR_weblinksportlet_siteUrl" type="text" style="width: 455px; margin: 9px 0px 5px 5px" autocomplete="off">
                                                                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> 
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="aui-column">
                                                <div class="aui-column-content">
                                                    <div class="aui-column">
                                                        <div class="aui-column-content"> 
                                                            <span class="label_box-prompt"> Tên website </span> 
                                                        </div>
                                                    </div>
                                                    <div class="aui-column">
                                                        <div class="aui-column-content">
                                                            <input type="text" name="webSiteName" class="input_box-prompt" id="webSiteName" style="width: 450px; margin: 9px 0px 5px 5px">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="siteId" id="siteId">
                                            <input type="hidden" id="iconNameOnAdd">
                                            <div class="box-prompt-control" style="clear: both; margin: 14px 4px 0 0">
                                                <span class="button-accept" id="btnAcceptDesktop">Đồng ý
                                                </span>
                                                <span class="button-cancel" id="btnCancel" style="margin-left: 0px">Bỏ qua
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="removeBoxPromptDialog" class="remove-box-prompt">
                                        <div class="box-prompt-header">
                                            <span id="boxRemovePromptDialogHeaderTitle"></span>
                                        </div>

                                        <div style="padding-top: 20px;">
                                            <p>
                                                Bạn muốn xóa địa chỉ website này khỏi I24h.vn?
                                            </p>
                                            <p>
                                                Tên website :
                                                <span id="webSiteRemoveName"></span>
                                            </p>
                                            <input type="hidden" name="siteRemoveId" id="siteRemoveId">
                                            <div>
                                                <p class="" style="float: left;">
                                                    Ảnh đại diện
                                                </p>
                                                <img style="margin-left: -345px;" id="icon-site-remove" src="http://i24h.vn/" width="25px" height="25px">
                                            </div>

                                            <div class="box-prompt-control" style="clear: both; margin: 14px 4px 0 0">
                                                <span class="button-accept" id="btnRemoveAccept" style="width: 83px">Đồng ý
                                                </span>
                                                <span class="button-cancel" id="btnRemoveCancel" style="margin-left: 0px">Bỏ qua
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div id="syncDialog" class="sync-box-prompt">
                                        <div class="box-prompt-header">
                                            <span id="syncDialogHeaderTitle"></span>
                                        </div>

                                        <div class="box-prompt-content">
                                            <span class="label_sync-dialog">
                                            Danh bạ hiện tại đã thay đổi so với danh bạ trong tài khoản của bạn.
                                        </span>

                                        <span class="label_sync-dialog">
                                            Chọn <strong>"Cập nhật"</strong>&nbsp;để cập nhật danh bạ hiện tại hoặc&nbsp;<strong>"Lấy về"</strong>&nbsp;để lấy lại danh bạ từ tài khoản của bạn.
                                        </span>


                                        <input type="hidden" name="syncHtmlData" id="syncHtmlData" value="">

                                        <div class="box-prompt-control">
                                            <span id="sync_btnAccept">
                                            </span>
                                            <span id="sync_btnCancel">
                                            </span>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="mainContent">
                                    <div class="col-sm-9 listLinkMain">
                                        @foreach ($main as $item)
                                        <div class="detailList">
                                            <div class="header-highlights">
                                                <div class='boxHeader'>
                                                    <a class="showLink" href="javascript:;">
                                                        <h2>{{$item->name}}</h2>
                                                    </a>
                                                </div>
                                                @foreach ($item->highlightLinks as $link)
                                                    <div class="highlights">
                                                        @php
                                                            $follow = ($link->is_follow == 0 || empty($link->is_follow)) ? 'nofollow' : 'follow';
                                                        @endphp
                                                        <a class="showLink" target="_blank" rel="{{$follow}}" href="{{$link->url}}">
                                                            <h2>{{$link->name}}</h2>
                                                        </a>
                                                    </div>
                                            @endforeach
                                            </div>
                                            <div class="normal-links">
                                            @foreach ($item->links as $link)
                                                <div class="col-md-2">
                                                    @php
                                                        $follow = ($link->is_follow == 0 || empty($link->is_follow)) ? 'nofollow' : 'follow';
                                                    @endphp
                                                    <a class="showLink" target="_blank" rel="{{$follow}}" href="{{$link->url}}">
                                                        <h2>{{$link->name}}</h2>
                                                    </a>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-3 listLinkLeft">
                                        @foreach ($left as $item)
                                            <ul class="detailLeft">
                                                <!-- <a href="/box/{{$item->id}}"><h2 class="boxHeader">{{$item->name}}</h2></a> -->
                                                <h2 class="boxHeader">{{$item->name}}</h2>
                                                @foreach ($item->links as $link)
                                                    <li class="col-md-12">
                                                        <a class="showLink" target="_blank" rel="{{$follow}}" href="{{$link->url}}">
                                                        @php
                                                            $follow = ($link->is_follow == 0 || empty($link->is_follow)) ? 'nofollow' : 'follow';
                                                        @endphp
                                                            <h2>{{$link->name}}</h2>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endforeach
                                    </div>
                                </div> <!-- main-content -->
                            </div>    
                        </div>    
                    </div>    
                </div>    
            </div>    
        </div>    
    </div>    
</div>    
@endsection