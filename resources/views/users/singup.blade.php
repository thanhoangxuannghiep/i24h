@extends('layouts.basic')

@section('content')
    <div class="login">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div id="loginCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="item">
                                <div class="panel panel-default panel-middle">
                                    <!-- <div class="panel-heading">Login</div> -->
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <img alt="i24h" class="logo" src="{{env('APP_LOGO')}}" data-holder-rendered="true" >
                                        </div>
                                        <h3 class="text-uppercase text-center m-b-0">{!! trans("titles.sign in") !!}</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ol class="carousel-indicators login-screen">
                                                    <li class="active"></li>
                                                    <li data-target="#loginCarousel" data-slide-to="1"></li>
                                                </ol>
                                            </div>
                                        </div>
                                        <admin-signin></admin-signin>
                                    </div>
                                    <div class="panel-footer text-center">
                                        {!! trans('titles.Do not have an account?') !!} <a class="text-uppercase" href="#loginCarousel" data-slide-to="1">{!! trans("titles.sign up") !!}</a>
                                    </div>
                                </div>
                            </div>

                            <div class="item active">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">Register</div> -->
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <img alt="i24h" class="logo" src="{{env('APP_LOGO')}}" data-holder-rendered="true" >
                                        </div>
                                        <h3 class="text-uppercase text-center m-b-0">{!! trans('titles.sign up') !!}</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ol class="carousel-indicators login-screen">
                                                    <li data-target="#loginCarousel" data-slide-to="0" ></li>
                                                    <li class="selected"></li>
                                                </ol>
                                            </div>
                                        </div>
                                        <user-register></user-register>
                                    </div>
                                    <div class="panel-footer text-center">
                                        {!! trans('titles.Have an account?') !!} <a class="text-uppercase" href="#loginCarousel" data-slide-to="0">{!! trans("titles.sign in") !!}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
