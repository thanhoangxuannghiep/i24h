@extends('layouts.basic')

@section('content')
<div class="login">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default panel-middle">
            <div class="panel-body">
                <div class="text-center">
                    <img alt="Dietary DNA" class="logo" src="{{env('APP_LOGO')}}" data-holder-rendered="true" >
                </div>
                <h3 class="text-uppercase text-center">New Password</h3>
                <user-reset email="{!! $email !!}" token="{!! $token !!}"></user-reset>
            </div>
            <div class="panel-footer text-center">
                Not have an account? <a href="/login" data-slide-to="1">SIGN UP</a>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
