@extends('layouts.basic')

@section('content')
<div class="login">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default panel-middle">
            <div class="panel-body">
                <div class="text-center">
                    <img alt="Dietary DNA" class="logo" src="{{env('APP_LOGO')}}" data-holder-rendered="true" >
                </div>
                <h3 class="text-uppercase text-center">Forgot Password</h3>
                <user-email></user-email>
            </div>
            <div class="panel-footer text-center">
                <a class="text-uppercase" href="/login" >Sign In</a>
            </div>
        </div>
    </div>
      </div>
    </div>
  </div>
</div>
@endsection