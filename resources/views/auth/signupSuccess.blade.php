@extends('layouts.basic')

@section('content')
    <div class="login">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div id="loginCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="panel panel-default panel-middle">
                                    <!-- <div class="panel-heading">Login</div> -->
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <img alt="i24h" class="logo" src="{{env('APP_LOGO')}}" data-holder-rendered="true" >
                                        </div>
                                        <h3 class="text-uppercase text-center m-b-0">{!! trans("titles.sign up success") !!}</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                            @php
                                                if($page){
                                                    echo $page->description;
                                                }
                                            @endphp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection