<?php
return [
    'SESSION_DOMAIN' => env('SESSION_DOMAIN', 'i24h.vn'),
    'user_type' => [
        1 => "User",
        3 => "Admin"
    ],
    'status' => [
        '1' => "Enable",
        '0' => "Disable",
        'true' => "Enable",
        'false' => "Disable",
    ],
    'follow' => [
        '1' => 'Follow',
        '0' => 'No Follow'
    ],
    'position' => [
        '1' => "Khu vực Top",
        '2' => "Khu Vực Main",
        '3' => "Khu Vực Left"
    ],
    'highlight' => [
        '1' => "Yes",
        '0' => "No"
    ]
];