-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 17, 2017 at 09:46 AM
-- Server version: 5.6.29
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `i24hhvn_home`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_slug` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_id` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `position` tinyint(4) NOT NULL,
  `priority` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `position`, `priority`, `status`, `created_at`, `updated_at`) VALUES
(12, 'Top', 'otpf', 1, 2, 1, '2017-09-12 06:55:40', '2017-09-12 22:52:47'),
(13, 'Link hay so 1', 'ff', 2, 1, 1, '2017-09-12 06:55:40', '2017-09-13 00:56:11'),
(14, 'Link huu ich so 1', 'd', 3, 1, 1, '2017-09-12 06:55:40', '2017-09-13 00:59:25'),
(15, 'Link hay so 2', 'f', 2, 2, 1, '2017-09-13 00:56:27', '2017-09-13 00:56:27'),
(16, 'Link hay so 3', 's', 2, 3, 1, '2017-09-13 00:57:24', '2017-09-13 00:57:24'),
(17, 'Link hay so 4', 'a', 2, 4, 1, '2017-09-13 00:59:07', '2017-09-13 00:59:07'),
(18, 'Link huu ich so 2', 'f', 3, 2, 1, '2017-09-13 00:59:47', '2017-09-13 00:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `title` varchar(254) COLLATE latin1_general_ci DEFAULT NULL,
  `url` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `logo` varchar(254) COLLATE latin1_general_ci DEFAULT NULL,
  `is_follow` tinyint(4) DEFAULT NULL,
  `category_id` tinyint(4) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `name`, `title`, `url`, `logo`, `is_follow`, `category_id`, `enable`, `created_at`, `updated_at`) VALUES
(4, 'google', 'Google', 'http://google.com', 'b1tKhFpmDF.png', 1, 12, 1, '2017-09-12 06:57:35', '2017-09-12 06:57:35'),
(5, 'facebook', 'Facebook', 'https://facebook.com', '7sZvYJJKYV.png', 1, 12, 1, '2017-09-12 06:59:04', '2017-09-12 23:34:32'),
(6, 'tweeter', 'Tweeter', 'http://tuyetroi.com', 'GfZ2PQoGex.png', 1, 12, 1, '2017-09-12 07:00:42', '2017-09-12 07:00:42'),
(7, 'Link s? 1', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(8, 'Link s? 2', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(9, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(10, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(11, 'Link s? 4', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(12, 'Link s? 5', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(13, 'Link s? 6', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(14, 'Link s? 7', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(15, 'Link s? 8', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(16, 'Link s? 9', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(17, 'Link s? 10', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(18, 'Link s? 11', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 13, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(19, 'Link s? 2', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(20, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(21, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(22, 'Link s? 4', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(23, 'Link s? 5', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(24, 'Link s? 6', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(25, 'Link s? 7', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(26, 'Link s? 8', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(27, 'Link s? 9', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(28, 'Link s? 10', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(29, 'Link s? 11', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 15, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(30, 'Link s? 2', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(31, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(32, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(33, 'Link s? 4', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(34, 'Link s? 5', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(35, 'Link s? 6', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(36, 'Link s? 7', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(37, 'Link s? 8', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(38, 'Link s? 9', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(39, 'Link s? 10', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(40, 'Link s? 11', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 16, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(41, 'Link s? 2', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(42, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(43, 'Link s? 3', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(44, 'Link s? 4', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(45, 'Link s? 5', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(46, 'Link s? 6', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(47, 'Link s? 7', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(48, 'Link s? 8', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(49, 'Link s? 9', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(50, 'Link s? 10', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(51, 'Link s? 11', 'Link s? 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 17, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(52, 'Link huu ich 2', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 14, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(53, 'Link huu ich 3', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 14, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(54, 'Link huu ich 3', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 14, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(55, 'Link huu ich 4', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 14, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(56, 'Link huu ich 5', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 14, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(57, 'Link huu ich 6', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 18, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(58, 'Link huu ich 7', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 18, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(59, 'Link huu ich 8', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 18, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(60, 'Link huu ich 9', 'Link huu ich 1', 'http://news.zing.vn/', 's1I3xtI0Tp.png', 1, 18, 1, '2017-09-12 23:48:47', '2017-09-12 23:48:47'),
(61, 'ádfdsaf', NULL, 'sadfsdaf', NULL, NULL, 12, 1, '2017-09-14 07:11:07', '2017-09-14 07:11:07'),
(62, 'ádfdsaf', NULL, 'sadfsdafdsf', NULL, NULL, 12, 1, '2017-09-14 07:12:07', '2017-09-14 07:12:07'),
(63, 'safdádf', NULL, 'sadfasf', NULL, NULL, 12, 1, '2017-09-14 07:12:20', '2017-09-14 07:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(65, '2014_10_12_000000_create_users_table', 1),
(66, '2014_10_12_100000_create_password_resets_table', 1),
(67, '2017_08_28_135047_create_table_user', 1),
(68, '2017_08_28_135053_create_table_link', 1),
(69, '2017_08_28_135100_create_table_box', 1),
(70, '2017_08_28_135106_create_table_page', 1),
(71, '2017_08_28_144042_create_table_category', 1),
(72, '2017_08_28_145045_create_table_article', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `url_slug` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `token` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `firstname` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `lastname` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(254) COLLATE latin1_general_ci NOT NULL,
  `remember_token` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `user_type`, `status`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'tony', 'chen', 3, 1, 'admin@gmail.com', '$2y$10$glRZq.HkEL58cNwSRGeJ7OWkxPCa.kKBtH4OvPppc16OKJTeQDCJW', 'fgXsEYAFlOSdeDRtEncrpmcmI0ThZDtba7YF4OnZzbFsjcy9dXvIv2qyCfgi', '2017-09-12 00:01:48', '2017-09-12 01:23:57'),
(2, 'sdaf s?dk', 'kjaslkdjf', 1, 1, 'jalkfdsj@gmail.com', '$2y$10$cxfyv3YUda32/cfjodHn4eW3XMmkey4krOH4SViB0YWuwKhpHj3yi', NULL, '2017-09-12 00:02:09', '2017-09-12 00:02:09'),
(3, 'sàdsda', 'ádfdsaf', 1, 1, 'admin@gmail.com1', '$2y$10$rLVMtIbTPTtXcseDHrPfee77EvpuIXimiuCTA8wj7glm3CprZ.DoK', 'POtP7r92zVQ4DeD0u6MuiLz8BA0ck2toZr8XGoEEniU2PI4odF0gBM5jDBYL', '2017-09-12 01:29:18', '2017-09-12 01:29:41'),
(4, 'à', 'j39u', 1, 1, 'klajf@gmail.com', '$2y$10$ahlbo6w2WenhZbn3zpbbjOp78lz/CcGBZNGJygCdx19X3n4.03IN2', NULL, '2017-09-12 01:29:31', '2017-09-12 01:29:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_name_unique` (`name`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
